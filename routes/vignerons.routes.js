const express = require('express');
const router = express.Router();

const vigneronController = require('../controller/vignerons.controller.js')

router.get('/', vigneronController.getAll)

router.get('/:id', vigneronController.getOne)

router.delete('/:id', vigneronController.delete)

router.post('/', vigneronController.addOne)

router.patch('/:id', vigneronController.updateOne)

module.exports = router;