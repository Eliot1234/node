const express = require('express');
const router = express.Router();

const vinController = require('../controller/vins.controller.js');

router.get('/', vinController.getAll);
router.get('/:id', vinController.getById);
router.get('/type/:type', vinController.getByType);
router.post('/', vinController.create);
router.patch('/:id', vinController.update);
router.delete('/:id', vinController.delete);

module.exports = router;