const faker = require('faker');
const Vigneron = require('./models/Vigneron');
const Vin = require('./models/Vin');

let doSeed = false;

if(doSeed)
    seed();

function seed() {

    for (i = 0; i < 200; i++) {

        let color = "Rouge";
        if(i>100)
        {
            color= "Blanc";
        }

        const vin = new Vin({
            nom: faker.name.lastName(),
            type: color,
            domaine: faker.name.firstName(),
            age: faker.datatype.number(10, 70),
            sepage: faker.name.firstName(),
            prix: faker.commerce.price(5,500,0)
        });
        vin.save();


        const vigneron = new Vigneron(
        {
            nom:  faker.name.lastName(),
            prenom:  faker.name.firstName(),
            age: faker.datatype.number(10, 70),
            ville: faker.address.cityName(),
        });
        vigneron.save();
    }
}