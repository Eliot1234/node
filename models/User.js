const mongoose = require("mongoose");

const schema = mongoose.Schema({
    login: String,
    password: String,
    token: String,
}, { collection: 'user' });

module.exports = mongoose.model("User", schema);