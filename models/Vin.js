const mongoose = require("mongoose");

const schema = mongoose.Schema({
    nom: String,
    type: String,
    domaine: String,
    age: Number,
    sepage: String
}, { collection: 'vin' });

module.exports = mongoose.model("Vin", schema);