const mongoose = require("mongoose");

const schema = mongoose.Schema({
    nom: String,
    prenom: String,
    age: Number,
    ville: String,
}, { collection: 'vigneron' });

module.exports = mongoose.model("Vigneron", schema);