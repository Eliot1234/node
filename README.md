# Démarrer l'api
- démarrer votre base de données. (partie bdd si dessous pour démarrer une base avec docker)
- npm install
- node index.js

.ENV a ajouter a la racine avec 2 variables

DB_CONNECTION="mongodb://localhost:27017/TP" (valeur a modifier selon votre url de connexion)

TOKEN_KEY = test

# Route
- GET localhost:27017/vin : liste de tous les vins 
- GET localhost:27017/vin/{id} : pour la recherche de vin par id
- GET localhost:27017/vin/type/{type} : pour la recherche de vin en fonction du type = Rouge ou Blanc
- DELETE localhost:27017/vin/{id} : pour delete un vin en fonction de l'id
- POST localhost:27017/vin : avec en parametere {"nom": "String","prenom": "String","age": 23,"ville": "String"} pour ajouter un vigneron
- PATCH localhost:27017/vin/{id} : avec en parametere {"nom": "String","prenom": "String","age": 23,"ville": "String"} pour modifier un vin en fonction de son id

- GET localhost:27017/vigneron : liste de tous les vignerons
- GET localhost:27017/vigneron/{id} : pour la recherche d'un vigneron par id
- DELETE localhost:27017/vigneron/{id} : pour delete un vigneron en fonction de l'id
- POST localhost:27017/vigneron : avec en parametere {"nom": "String","type": "String","domaine": "String","age": 20,"sepage": "String"} pour ajouter un vin
- PATCH localhost:27017/vigneron/{id} : avec en parametere {"nom": "String","type": "String","domaine": "String","age": 20,"sepage": "String"} pour modifier un vigneron en fonction de son id

- localhost:27017/user/register : avec en parametere {"login": "String","password": "String"} pour creer un user
- localhost:27017/user/login : avec en parametere {"login": "String","password": "String"} pour se connecter et avoir le token d'authentification

# Base de donnée
mongoDB avec docker
commande docker : docker run --name mongodb -d -p 27017:27017 mongo
utilisation de MongoDB Compass avec bdd qui s'appelle TP qui contient trois collections : vin, vigneron et user.

# Fausse donnée
Au moment du démarrage de l'api des fausses données sont générée
Pour desactiver cette génération au démarage dans le fichier seeder.js ligne 5 passer doSeed a false
Pour activer cette génération au démarage dans le fichier seeder.js ligne 5 passer doSeed a true

# Librairie Utilisé
- mongoose
- faker-js
- rooter
- express
- json web token
- bcryptjs
- dotenv
- esm

    faker permet de générer de fausses données par thème (nom de famille, prénom, adresse, ville ...). 
    Pour plus d'informations :https://fakerjs.dev/guide/#node-js

    esm permet d'utiliser les nouvelles méthodes d'import (import { sep } from 'node:path') 
    Pour plus d'informations : https://nodejs.org/api/esm.html

# Postman
fichier postman disponible a la racine du projet : "NodeJs Vin et Vignerons.postman_collection.json"
avec l'ensemble des requetes lié aux routes