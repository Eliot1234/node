const auth = require("./middleware/auth");
const express = require('express');
//création de l'app express
const app = express();
app.use(express.json());
const port = 3001;

//liste des routes
const routeVin = require('./routes/vins.routes')
const routeVigneron = require('./routes/vignerons.routes');
const routeUser = require('./routes/user.routes');

require('./config/database');
require('./seeder');

app.use('/user', routeUser);
app.use('/vin', auth, routeVin);
app.use('/vigneron',auth, routeVigneron);

app.listen(port, () => {
    console.log(`server is running on port ${port}`);
});



