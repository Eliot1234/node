const Vigneron = require('../models/Vigneron')

exports.getVignerons = async function(query) {
    try {
        const vignerons = Vigneron.find(query)
        return vignerons
    } catch (err) {
        // Bad request 400 error
    }
}

exports.deleteVignerons = async function(query) {
    try {
        const vigneron = Vigneron.deleteOne(query)
        return vigneron
    } catch (err) {
        // Bad request 400 error
    }
}

exports.addVigneron = async function(query) {
    try {
        const oneVigneron = new Vigneron({
            nom: query.nom,
            prenom: query.prenom,
            age: query.age,
            ville: query.ville,
        });
        const vigneron = await oneVigneron.save()
        return vigneron
    } catch (err) {
        // Bad request 400 error
    }
}

exports.updateVigneron = async function(id, query) {
    try {
        const vigneron = Vigneron.findByIdAndUpdate(id, query);
        return vigneron
    } catch (err) {
        // Bad request 400 error
    }
}   