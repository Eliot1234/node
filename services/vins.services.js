const Vin = require('../models/Vin')

exports.getVins = (async (query) => {
    try {
        const vins = Vin.find(query);
        return vins;
    } catch (err) {
        // Bad request 400 error
    }
});

exports.getById = (async (id) => {
    try {
        const vin = await Vin.find({ _id: id });

        return vin;
    } catch (err) {
        // Bad request 400 error
    }
});

exports.create = (async (body) => {
    try {
        const vin = new Vin({
            nom: body.nom,
            type: body.type,
            domaine: body.domaine,
            age: body.age,
            sepage: body.sepage
        });
        await vin.save();
        return vin;
    } catch (err) {
        // Bad request 400 error
    }
});

exports.delete = (async (id) => {
    try {
        const vin = await Vin.deleteOne({ _id:id });
        return vin;
    }
    catch(error)
    {

    }
});

exports.update = (async (id,query) => {
    try{
        var vin = await Vin.findByIdAndUpdate(id,query);
        return vin;
    }
    catch(error){
    }
});