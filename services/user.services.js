const User = require('../models/User')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.login = async function(req) {
    try {
        // Get user input
        const { login, password } = req.body;


        // Validate user input
        if (!(login && password)) {
            console.log("All input is required");
        }
        // Validate if user exist in our database
        const user = await User.findOne({ login });

        if (user && (await bcrypt.compare(password, user.password))) {
            // Create token
            const token = jwt.sign(
                { user_id: user._id, login },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h",
                }
            );

            // save user token
            user.token = token;
            user.save();
            // user
            return user;
        }
        else {
            console.log("Invalid Credentials");
        }
    } catch (err) {
        console.log(err);
    }
}

exports.register = async (req) => {
    try {
        // Get user input
        const { login, password } = req.body;

        // Validate user input
        if (!(login && password)) {
            console.log("All input is required");
        }

        // check if user already exist
        // Validate if user exist in our database
        const oldUser = await User.findOne({ login });

        if (oldUser) {
            console.log("User Already Exist. Please Login");
        }

        //Encrypt user password
        encryptedPassword = await bcrypt.hash(password, 10);

        // Create user in our database
        const user = await User.create({
            login,
            password: encryptedPassword,
        });

        // Create token
        const token = jwt.sign(
            { user_id: user._id, login },
            process.env.TOKEN_KEY,
            {
                expiresIn: "2h",
            }
        );
        // save user token
        user.token = token;

        // return new user
        return(user);
    } catch (err) {
        console.log(err);
    }
}