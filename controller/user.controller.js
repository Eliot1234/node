const User = require('../models/User.js');
var UserService = require('../services/user.services.js')    

exports.login = async function (req, res) {
    try {
        var user = await UserService.login(req);
        
        if(!user)
        {
            return res.status(200).json({ message: "Wrong username or password" });
        }

        return res.status(200).json({ status: 200, data: user, message: "Succesfully connected" });

    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.register = async function (req, res) {
    try {
        var user = await UserService.register(req);
        return res.status(200).json({ status: 200, data: user, message: "Account successfully registered" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

