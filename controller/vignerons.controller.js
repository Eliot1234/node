const Vigneron = require('../models/Vigneron.js');
var vigneronService = require('../services/vignerons.services.js')    

exports.getAll = async function (req, res) {
    try {
        var vignerons = await vigneronService.getVignerons({})
        return res.status(200).json({ status: 200, data: vignerons, message: "Succesfully Vignerons Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.getOne = async function (req, res) {
    try {
        var vignerons = await vigneronService.getVignerons({ _id: req.params.id })
        return res.status(200).json({ status: 200, data: vignerons, message: "Succesfully Vigneron Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.delete = async function (req, res) {
    try {
        var vigneron = await vigneronService.deleteVignerons({ _id: req.params.id })
        return res.status(200).json({ status: 200, data: vigneron, message: "Succesfully Vigneron Deleted" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.addOne = async function (req, res) {
    try {
        var vignerons = await vigneronService.addVigneron(req.body)
        return res.status(200).json({ status: 200, data: vignerons, message: "Succesfully Vigneron Added" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.updateOne = async function (req, res) {
    try {
        var vigneron = await vigneronService.updateVigneron(req.params.id, req.body)
        return res.status(200).json({ status: 200, data: vigneron, message: "Succesfully Vigneron Update" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}