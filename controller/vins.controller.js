const id = require('faker/lib/locales/id_ID/index.js');
var vinService = require('../services/vins.services.js')

exports.getAll = (async (req, res) => {
    try {
        var vins = await vinService.getVins({})
        return res.status(200).json({ status: 200, data: vins, message: "Succesfully Vins Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.getWhiteWine = (async (req, res) => {
    try {
        const vin = await vinService.getVins({ "type": "Blanc" });
        return res.status(200).json({ status: 200, data: vin, message: "Succesfully white wine retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.getRedWine = (async (req, res) => {
    try {
        const vin = await vinService.getVins({ "type": "Rouge" });
        return res.status(200).json({ status: 200, data: vin, message: "Succesfully white wine retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.getById = (async (req, res) => {
    try {
        const vin = await vinService.getVins({ _id: req.params.id })
        return res.status(200).json({ status: 200, data: vin, message: "Succesfully Vin Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.getByType = (async (req, res) => {
    try {
        const vin = await vinService.getVins({ type: req.params.type })
        return res.status(200).json({ status: 200, data: vin, message: "Succesfully Vin Retrieved" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.create = (async (req, res) => {
    try {
        const vin = await vinService.create(req.body)
        return res.status(200).json({ status: 200, data: vin, message: "Succes" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.delete = (async (req, res) => {
    try {
        const vin = await vinService.delete(req.params.id);
        return res.status(200).json({ status: 200, data: vin, message: "Succes" });
    } catch (error) {
        return res.status(400).json({ status: 400, message: e.message });
    }
});

exports.update = (async (req, res) => {
    try {
        const vin = await vinService.update(req.params.id,req.body);
        return res.status(200).json({ status: 200, data: vin});
    }
    catch (error) {
        return res.status(400).json({ status: 400, message: e});
    }
});



