require('dotenv').config();
// Fichier de connexion a la base de données 
var mongodb = process.env.DB_CONNECTION;

const mongoose = require('mongoose');

mongoose.connect(mongodb,);

const database = mongoose.connection;

database.on('error', console.error.bind(console.log("db error")));

database.on('open', () => {
    console.log('db connected');
});
